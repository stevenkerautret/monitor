package monitor;

public class Consumer implements Runnable {
	public Monitor monitor;
	
	public Consumer(Monitor pMonitor){
		monitor = pMonitor;
	}
	
	public void run() {
		while (!monitor.getStatus()){
			monitor.read();
		}
	}

}
