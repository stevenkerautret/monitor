package monitor;


public class Producer implements Runnable{
	public String[] list = {"A","B","C","D","E","F","G","H","I","J"};
	public Monitor monitor;

	public Producer(Monitor pMonitor) {
		monitor = pMonitor;
	}
	public void run() {
		int i;
		
		for(i=0; i<list.length; i++){
			monitor.write(list[i]);
		}
		monitor.setStatus();
	}

}
