package monitor;

public class Monitor {
	public String buffer;
	public boolean available;
	public boolean dead;
	
	public Monitor (){
		buffer = "";
		available = false;
		dead = false;
	}
	
	public synchronized void read(){
		while(!available){
			try{
				wait();
			}catch(InterruptedException e){
				System.out.println(e);
			}
		}
		System.out.println(buffer);
		available = false;
		notifyAll();
	}
	
	public synchronized void write(String str){
		while(available){
			try{
				wait();
			}catch(InterruptedException e){
				System.out.println(e);
			}
		}
		buffer = str;
		available = true;
		notifyAll();
	}
	
	public boolean getStatus(){
		return dead;
	}
	public void setStatus(){
		dead = true;
	}
}
