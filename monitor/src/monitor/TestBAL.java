package monitor;

public class TestBAL {
	public static Monitor monitor;
	
	public static void main(String[] args) {
		monitor = new Monitor();
		Producer prod = new Producer(monitor);
		Thread tProd = new Thread(prod);
		
		Consumer cons = new Consumer(monitor);
		Thread tCons = new Thread(cons);
		
		tProd.start();
		tCons.start();
		//monitor.write("test");
	}

}
